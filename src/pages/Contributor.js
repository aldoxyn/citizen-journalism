import React, { Component } from 'react'
import { Image, ToastAndroid, Alert, FlatList } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Text, Container, Header, Left, Icon, Body, Right, Button, Content, Item, Card, CardItem, View, Spinner } from 'native-base'
import { token } from '../redux/action/TokenAction'
import { connect } from 'react-redux'
import Axios from 'axios'

const mapStateToProps = state => ({
    token: state.token.token,
    user: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: data => dispatch(token(data))
    };
};

class Contributor extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: false,
            data: [
                {
                    _id: {
                        fullname: 'Loading',
                        image: {
                            secure_url: "https://res.cloudinary.com/limkaleb/image/upload/v1570870518/citizen-journalism/f6wot9wblkdiizfjxbws.jpg"
                        },
                        subscribers: [],
                        _id: 1
                    },
                    news: [],
                    totalNews: 100
                }
            ],
            subbed: [],
            loading: true,
            refreshing: false
        }
    }

    scan

    componentDidMount() {
        this.contributor()
        this.scan = setInterval(this.check_sub, 1000)
    }

    handleRefresh = () => {
        this.setState({
            refreshing: true
        },
            () => {
                this.contributor()
            }
        )
    }

    contributor = () => {
        const api = async () => await Axios.get(
            'https://app-citizenjournalism.herokuapp.com/api/v1/user/contributors'
        )

        api()
            .then(res => {
                this.setState({
                    data: res.data.result
                })
            })
    }

    sub = (id, i) => {
        const api = async () => await Axios.post(
            `https://app-citizenjournalism.herokuapp.com/api/v1/subs/${id}`,
            null,
            {
                headers: {
                    Authorization: `Bearer ${this.props.token}`
                }
            }
        )
        api()
            .then(res => {
                this.handleRefresh()
                ToastAndroid.show('Subscribed!', ToastAndroid.SHORT)
            })
    }

    unsub = (id, i) => {
        const api = async () => await Axios.delete(
            `https://app-citizenjournalism.herokuapp.com/api/v1/subs/${id}`,
            {
                headers: {
                    Authorization: `Bearer ${this.props.token}`
                }
            }
        )

        api()
            .then(res => {
                this.handleRefresh()
                ToastAndroid.show('Unsubscribed!', ToastAndroid.SHORT)
            })
    }

    check_sub = () => {
        const api = async (id) => await Axios.get(
            `https://app-citizenjournalism.herokuapp.com/api/v1/subs/${id}`,
            {
                headers: {
                    Authorization: `Bearer ${this.props.token}`
                }
            }
        )

        if (this.state.data.length >= 2) {
            if (this.props.token !== 'null') {
                this.state.data.map((value, i) => {
                    api(value._id._id)
                        .then(res => {
                            this.state.subbed[i] = res.data.result
                            this.setState({
                                loading: false,
                                refreshing: false
                            })
                        })
                })
            } else {
                this.setState({
                    loading: false,
                    refreshing: false
                })
            }
        }
    }

    render() {
        if (this.state.loading) {
            return (
                <Container>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignContent: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Container>
            )
        } else {
            return (
                <Container>
                    <Header
                        style={{
                            display: 'flex',
                            backgroundColor: '#FFF'
                        }}
                    >
                        <Left
                            style={{
                                flex: 1
                            }}
                        >
                            <Text
                                style={{
                                    color: '#FFF'
                                }}
                            > Hho</Text>
                        </Left>
                        <Body
                            style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <Text style={{ fontWeight: "bold" }}>
                                Contributor
                    </Text>
                        </Body>
                        <Right
                            style={{
                                flex: 1
                            }}
                        >
                            <Icon
                                type="Ionicons"
                                name="md-exit"
                                style={{
                                    color: '#FFF'
                                }}
                            />
                        </Right>
                    </Header>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'column'
                        }}
                    >
                        <Item
                            style={{
                                flexDirection: 'column',
                                borderColor: 'transparent'
                            }}
                        >
                            <Image
                                source={{ uri: this.state.data[0]._id.image.secure_url }}
                                style={{
                                    width: 100,
                                    height: 100,
                                    borderRadius: 100 / 2,
                                    marginVertical: 10
                                }}
                            />
                            <Text>
                                {this.state.data[0]._id.fullname}
                            </Text>
                            <Text>
                                Top Contributor of the Month
                            </Text>
                        </Item>
                        <FlatList
                            data={this.state.data}
                            renderItem={({ item, index }) => (
                                <Card
                                    transparent
                                    key={item._id._id}
                                >
                                    <CardItem
                                        button
                                        onPress={
                                            () => this.props.navigation.navigate('Profile', { id: item._id._id })
                                        }
                                    >
                                        <Body
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'center',
                                                alignItems: 'center'
                                            }}
                                        >
                                            <Image
                                                source={{ uri: item._id.image.secure_url }}
                                                style={{
                                                    width: 50,
                                                    height: 50,
                                                    borderRadius: 50 / 2
                                                }}
                                            />
                                            <Item
                                                style={{
                                                    flex: 1,
                                                    flexDirection: 'column',
                                                    alignItems: 'flex-start',
                                                    marginLeft: 10,
                                                    borderBottomColor: 'transparent',
                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        fontWeight: 'bold'
                                                    }}
                                                >
                                                    {item._id.fullname}
                                                </Text>
                                                <Text>Total CitiNews: {item.totalNews}</Text>
                                                <Text>Total Subscriber: {item._id.subscribers.length}</Text>
                                            </Item>
                                        </Body>
                                        <Right>
                                            <Button
                                                style={{
                                                    width: '80%',
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    backgroundColor: this.state.subbed[index] ? '#BBBBBB' : 'black'
                                                }}
                                                onPress={
                                                    () => {
                                                        if (this.props.token !== 'null') {
                                                            if (this.state.subbed[index]) {
                                                                Alert.alert(
                                                                    'Unsubscribe?',
                                                                    'Are you sure want to unsubscribe ?',
                                                                    [
                                                                        {
                                                                            text: 'No',
                                                                            onPress: () => ToastAndroid.show('Cancelled', ToastAndroid.SHORT),
                                                                        },
                                                                        { text: 'Yes', onPress: () => this.unsub(item._id._id, index) },
                                                                    ],
                                                                    { cancelable: true },
                                                                );
                                                            } else {
                                                                this.sub(item._id._id, index)
                                                            }
                                                        } else {
                                                            this.props.navigation.navigate('Login')
                                                        }
                                                    }
                                                }
                                            >
                                                <Text>{this.state.subbed[index] ? 'Subscribed' : 'Subscribe'}</Text>
                                            </Button>
                                        </Right>
                                    </CardItem>
                                </Card>
                            )}
                            keyExtractor={item => item._id._id}
                            onRefresh={this.handleRefresh}
                            refreshing={this.state.refreshing}
                        />
                    </View>
                </Container >
            )
        }
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Contributor))