import React, { Component } from 'react'
import { Image, Dimensions } from 'react-native'
import { Text, View, Card, CardItem, Body, Container, Content, Header, Left, Icon, Right } from 'native-base'
import { withNavigation } from 'react-navigation'

class News_List extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.navigation.getParam('data', 'null')
        }
    }

    render() {
        const loops = this.state.data.map(value => {
            if (this.props.navigation.getParam('page') === 'All News') {
                return (
                    <Card
                        key={value._id}
                    >
                        <Image
                            source={{ uri: value.media.secure_url }}
                            style={{
                                width: Dimensions.get('window').width,
                                height: 200,
                                alignSelf: 'center'
                            }}
                        />
                        <CardItem button onPress={
                            () => {
                                this.props.navigation.navigate('News', {
                                    id: value._id
                                })
                            }
                        }>
                            <Body>
                                <Text
                                    style={{
                                        fontSize: 28,
                                        fontFamily: 'Playfair'
                                    }}
                                >
                                    {value.title}
                                </Text>
                                <Text note
                                    style={{
                                        fontSize: 14,
                                        fontFamily: 'Playfair'
                                    }}
                                >
                                    {`${value.description.substring(0, 127)}...`}
                                </Text>
                            </Body>
                        </CardItem>
                    </Card>
                )
            } else if (this.props.navigation.getParam('page') === 'Approved') {
                if (value.status == 'Approved') {
                    return (
                        <Card
                            key={value._id}
                        >
                            <Image
                                source={{ uri: value.media.secure_url }}
                                style={{
                                    width: Dimensions.get('window').width,
                                    height: 200,
                                    alignSelf: 'center'
                                }}
                            />
                            <CardItem button onPress={
                                () => {
                                    this.props.navigation.navigate('News', {
                                        id: value._id
                                    })
                                }
                            }>
                                <Body>
                                    <Text
                                        style={{
                                            fontSize: 28,
                                            fontFamily: 'Playfair'
                                        }}
                                    >
                                        {value.title}
                                    </Text>
                                    <Text note
                                        style={{
                                            fontSize: 14,
                                            fontFamily: 'Playfair'
                                        }}
                                    >
                                        {`${value.description.substring(0, 127)}...`}
                                    </Text>
                                </Body>
                            </CardItem>
                        </Card>
                    )
                }
            } else if (this.props.navigation.getParam('page') === 'Pending') {
                if (value.status == 'Pending') {
                    return (
                        <Card
                            key={value._id}
                        >
                            <Image
                                source={{ uri: value.media.secure_url }}
                                style={{
                                    width: Dimensions.get('window').width,
                                    height: 200,
                                    alignSelf: 'center'
                                }}
                            />
                            <CardItem button onPress={
                                () => {
                                    this.props.navigation.navigate('News', {
                                        id: value._id
                                    })
                                }
                            }>
                                <Body>
                                    <Text
                                        style={{
                                            fontSize: 28,
                                            fontFamily: 'Playfair'
                                        }}
                                    >
                                        {value.title}
                                    </Text>
                                    <Text note
                                        style={{
                                            fontSize: 14,
                                            fontFamily: 'Playfair'
                                        }}
                                    >
                                        {`${value.description.substring(0, 127)}...`}
                                    </Text>
                                </Body>
                            </CardItem>
                        </Card>
                    )
                }
            }
        })
        return (
            <Container>
                <Header
                    style={{
                        backgroundColor: 'white'
                    }}
                >
                    <Left
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center'
                        }}
                    >
                        <Icon
                            type='Ionicons'
                            name='ios-arrow-back'
                            style={{
                                color: '#3083C4'
                            }}
                            onPress={
                                () => this.props.navigation.goBack()
                            }
                        />
                        <Text
                            style={{
                                marginLeft: 5,
                                color: '#3083C4'
                            }}
                            onPress={
                                () => this.props.navigation.goBack()
                            }
                        >
                            Profile
                        </Text>
                    </Left>
                    <Body
                        style={{
                            flex: 1
                        }}
                    >
                        <Text
                            style={{
                                justifyContent: 'center',
                                alignSelf: 'center'
                            }}
                        >
                            {this.props.navigation.getParam('page', 'null')}
                        </Text>
                    </Body>
                    <Right
                        style={{
                            flex: 1
                        }}
                    >
                        <Text
                            style={{
                                color: 'white'
                            }}
                        > wjwjwj</Text>
                    </Right>
                </Header>
                <Content>
                    {loops}
                </Content>
            </Container>
        )
    }
}
export default withNavigation(News_List)