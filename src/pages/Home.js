import React, { Component } from 'react'
import { Image, ToastAndroid, Modal, Dimensions, FlatList } from 'react-native'
import { Text, Container, Card, CardItem, Body, Content, View, Spinner } from 'native-base'
import { withNavigation } from 'react-navigation'
import { token } from '../redux/action/TokenAction'
import { connect } from 'react-redux'
import firebase from 'react-native-firebase'
import * as Storage from '../functions/async'
import Axios from 'axios'
import Header_home from '../components/Header_home'

const mapStateToProps = state => ({
    token: state.token.token,
    user: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: data => dispatch(token(data))
    };
};

class Home extends Component {

    constructor(props) {
        super(props)
        this.state = {
            refreshing: false,
            loading: true,
            data: [
                {
                    _id: '09',
                    title: 'empty',
                    description: 'empty',
                    media: {
                        secure_url: 'empty'
                    }
                },
            ]
        }
    }

    componentDidMount() {
        firebase.messaging().hasPermission()
            .then(enabled => {
                if (enabled) {
                    // user has permissions
                    firebase.messaging().getToken()
                        .then(fcmToken => {
                            if (fcmToken) {
                                // user has a device token
                                console.log(fcmToken)
                            } else {
                                // user doesn't have a device token yet
                            }
                        });
                } else {
                    // user doesn't have permission
                    firebase.messaging().requestPermission()
                        .then(() => {
                            // User has authorised  
                        })
                        .catch(error => {
                            // User has rejected permissions  
                        });
                }
            });
        this.ambildata()
    }

    ambildata = async () => {
        const data = async () => await Axios.get(
            'https://app-citizenjournalism.herokuapp.com/api/v1/news/status/approved'
        )

        const tokens = await Storage.getData('token')
        this.props.FetchToken(tokens)

        data()
            .then(res => {
                this.setState({
                    refreshing: false,
                    loading: false,
                    data: res.data.result
                })
            })
    }

    handleRefresh = () => {
        this.setState({
            refreshing: true,
            data: []
        },
            () => {
                this.ambildata()
            }
        )
    }

    render() {
        return (
            <Container>
                <View
                    style={{
                        flex: 1, backgroundColor: '#DFDFDF'
                    }}
                >
                    <Header_home />
                    <FlatList
                        data={this.state.data}
                        renderItem={({ item }) => (
                            <Card
                                key={item._id}
                            >
                                <Image
                                    source={{ uri: item.media.secure_url }}
                                    style={{
                                        width: Dimensions.get('window').width,
                                        height: 200,
                                        alignSelf: 'center'
                                    }}
                                />
                                <CardItem button onPress={
                                    () => {
                                        this.props.navigation.navigate('News', {
                                            id: item._id
                                        })
                                    }
                                }>
                                    <Body>
                                        <Text
                                            style={{
                                                fontSize: 24,
                                                fontFamily: 'Playfair'
                                            }}
                                        >
                                            {item.title}
                                        </Text>
                                        <Text note
                                            style={{
                                                marginVertical: 5,
                                                fontSize: 14,
                                                fontFamily: 'Playfair'
                                            }}
                                        >
                                            {`${item.description.substring(0, 127)}...`}
                                        </Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        )}
                        keyExtractor={item => item._id}
                        onRefresh={this.handleRefresh}
                        refreshing={this.state.refreshing}
                    />
                </View>
                <Modal
                    animationType='fade'
                    transparent={true}
                    visible={this.state.loading}
                    onRequestClose={
                        () => ToastAndroid.show('Wait a minute!', ToastAndroid.SHORT)
                    }
                >
                    <View
                        style={{
                            backgroundColor: 'rgba(0,0,0,1)',
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Modal>
            </Container>
        )
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Home))