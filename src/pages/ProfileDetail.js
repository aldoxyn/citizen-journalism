import React, { Component } from 'react'
import { Image } from 'react-native'
import { View, Text, Container, Content, Icon, Left, Item, Right, Card, CardItem, Button, Fab, Spinner, List, ListItem, Body } from 'native-base'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'
import { getUser } from '../redux/action/user'
import Axios from 'axios'
import Header_Logout from '../components/Header_Logout'

const mapStateToProps = state => ({
    token: state.token,
    user: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        GetUser: token => dispatch(getUser(token))
    };
};

class ProfileDetail extends Component {

    constructor(props) {
        super(props)
        this.state = {
            active: true,
            profile: {
                user: {
                    news: []
                }
            }
        }
    }

    componentDidMount() {
        this.profile()
    }

    profile = () => {
        const api = async () => Axios.get(
            `https://app-citizenjournalism.herokuapp.com/api/v1/user/details/${this.props.navigation.getParam('id', null)}`
        )

        api()
            .then(res => {
                this.setState({
                    active: false,
                    profile: res.data.result
                })
            })
    }

    color = (status) => {
        if (status === 'Pending') {
            return '#fff000'
        } else if (status === 'Approved') {
            return '#00CC21'
        } else if (status === 'Rejected') {
            return '#ff0000'
        }
    }

    render() {
        const loop = this.state.profile.user.news.map(value => {
            return (
                <ListItem
                    key={value._id}
                    onPress={
                        () => {
                            this.props.navigation.navigate('News', {
                                id: value._id
                            })
                        }
                    }
                >
                    <Left
                        style={{
                            marginLeft: '5%'
                        }}
                    >
                        <Image
                            source={{
                                uri: value.media.secure_url
                            }}
                            style={{
                                width: 180,
                                height: 120
                            }}
                        />
                    </Left>
                    <Body
                        style={{
                            marginRight: -20
                        }}
                    >
                        <Text>
                            {value.date.substring(0, 10)}
                        </Text>
                        <Text
                            style={{
                                fontWeight: 'bold'
                            }}
                        >
                            {value.title}
                        </Text>
                    </Body>
                </ListItem>
            )
        })
        if (this.state.active) {
            return (
                <Container>
                    <View
                        style={{
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Container>
            )
        } else {
            return (
                <Container>
                    <Header_Logout />
                    <Item
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginVertical: 20,
                            borderColor: 'transparent'
                        }}
                    >
                        <Image
                            source={{ uri: this.state.profile.user.image.secure_url }}
                            style={{ width: 100, height: 100, borderRadius: 100 / 2 }}
                        />
                        <Text style={{ marginHorizontal: 50 }}>{`${this.state.profile.user.news.length} News`}</Text>
                        <Text>{`${this.state.profile.total_subscribers} Subscribers`}</Text>
                    </Item>
                    <Text style={{ marginLeft: '5%', fontSize: 20, marginBottom: '5%' }}>{this.state.profile.user.fullname}</Text>
                    <Content>
                        <List style={{ marginLeft: -20 }}>
                            <ListItem style={{ backgroundColor: '#E7E7E7' }} first>
                                <Text style={{ marginLeft: '10%' }}>NEWS</Text>
                            </ListItem>
                            {loop}
                        </List>
                    </Content>
                </Container >
            )
        }
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(ProfileDetail))