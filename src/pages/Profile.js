import React, { Component } from 'react'
import { Image, ToastAndroid, FlatList } from 'react-native'
import { View, Text, Container, Content, Icon, Left, Item, Right, Card, CardItem, Button, Fab, Spinner, List, ListItem } from 'native-base'
import { withNavigation } from 'react-navigation'
import { token } from '../redux/action/TokenAction'
import { connect } from 'react-redux'
import { getUser } from '../redux/action/user'
import Axios from 'axios'
import Header_Logout from '../components/Header_Logout'
import Login from './Login'
import * as Storage from '../functions/async'

const mapStateToProps = state => ({
    token: state.token.token,
    user: state.user.data,
    raw: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: data => dispatch(token(data)),
        GetUser: token => dispatch(getUser(token))
    };
};

class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: false,
            loading: true,
            refreshing: false
        }
    }

    componentDidMount() {
        this.props.GetUser(this.props.token)
    }

    handleRefresh = () => {
        this.setState({
            refreshing: true,
        },
            () => {
                this.props.GetUser(this.props.token)
            }
        )
    }

    render() {
        if (this.props.token !== 'null') {
            return (
                <Container>
                    <Header_Logout />
                    <Text style={{ fontWeight: 'bold', marginLeft: 30, marginVertical: 10 }}>{this.props.user.user.fullname}</Text>
                    <FlatList
                        data={[this.props.user.user]}
                        renderItem={({ item }) => (
                            <View>
                                <Item
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginVertical: 20,
                                        borderColor: 'transparent'
                                    }}
                                >
                                    <Image
                                        source={{ uri: this.props.user.user.image.secure_url }}
                                        style={{ width: 100, height: 100, borderRadius: 100 / 2 }}
                                    />
                                    <Text style={{ marginHorizontal: 50 }}>{`${this.props.user.user.news.length} News`}</Text>
                                    <Text>{`${this.props.user.total_subscribers} Subscribers`}</Text>
                                </Item>
                                <List style={{ marginLeft: -15 }} key={item._id}>
                                    <ListItem style={{ backgroundColor: '#E7E7E7' }} first>
                                        <Text>ACCOUNT</Text>
                                    </ListItem>
                                    <ListItem>
                                        <Text
                                            onPress={
                                                () => {
                                                    Storage.setData('token', 'null')
                                                    this.props.FetchToken('null')
                                                    ToastAndroid.show('Loggedout', ToastAndroid.SHORT)
                                                    this.props.navigation.goBack()
                                                }
                                            }
                                        >
                                            Log Out ({item.fullname})
                                </Text>
                                    </ListItem>
                                    <ListItem
                                        onPress={
                                            () => this.props.navigation.navigate('Edit')
                                        }
                                        last
                                    >
                                        <Left>
                                            <Text>Edit Profile</Text>
                                        </Left>
                                        <Right>
                                            <Icon name='arrow-forward' />
                                        </Right>
                                    </ListItem>
                                    <ListItem style={{ backgroundColor: '#E7E7E7' }}>
                                        <Text>MY NEWS</Text>
                                    </ListItem>
                                    <ListItem
                                        onPress={
                                            () => this.props.navigation.navigate('News_list', { data: item.news, page: 'All News' })
                                        }
                                    >
                                        <Left>
                                            <Text>All News</Text>
                                        </Left>
                                        <Right>
                                            <Icon name='arrow-forward' />
                                        </Right>
                                    </ListItem>
                                    <ListItem
                                        onPress={
                                            () => this.props.navigation.navigate('News_list', { data: item.news, page: 'Approved' })
                                        }
                                    >
                                        <Left>
                                            <Text>Published News</Text>
                                        </Left>
                                        <Right>
                                            <Icon name='arrow-forward' />
                                        </Right>
                                    </ListItem>
                                    <ListItem
                                        onPress={
                                            () => this.props.navigation.navigate('News_list', { data: item.news, page: 'Pending' })
                                        }
                                    >
                                        <Left>
                                            <Text>Pending News</Text>
                                        </Left>
                                        <Right>
                                            <Icon name='arrow-forward' />
                                        </Right>
                                    </ListItem>
                                    <ListItem style={{ backgroundColor: '#E7E7E7' }}>
                                        <Text></Text>
                                    </ListItem>
                                    <ListItem
                                        onPress={
                                            () => this.props.navigation.navigate('AboutUs')
                                        }
                                    >
                                        <Text>About</Text>
                                    </ListItem>
                                </List>
                            </View>
                        )}
                        keyExtractor={item => item._id}
                        onRefresh={this.handleRefresh}
                        refreshing={this.props.raw.loading}
                    />
                </Container>
            )
        } else {
            return <Login />
        }
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Profile))