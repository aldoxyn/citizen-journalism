import React, { Component } from 'react'
import { Text, View, Image, Modal, ToastAndroid } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Spinner, Container, Content, Button, Item, Icon, Card, CardItem, Input, Picker } from 'native-base'
import Edit_Header from '../components/Edit_Header'
import { token } from '../redux/action/TokenAction'
import { connect } from 'react-redux'
import { getUser } from '../redux/action/user'
import Axios from 'axios'
import * as Storage from '../functions/async'
import ImagePicker from 'react-native-image-picker'

const mapStateToProps = state => ({
    token: state.token,
    user: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: data => dispatch(token(data)),
        GetUser: token => dispatch(getUser(token))
    };
};

class Edit extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: this.props.user.data.user.email,
            fullname: this.props.user.data.user.fullname,
            gender: this.props.user.data.user.gender,
            address: this.props.user.data.user.address,
            picture: this.props.user.data.user.image.secure_url,
            Modal: false
        }
    }

    Pict_Prof = () => {
        const options = {
            title: 'Select Profile!',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        }

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                ToastAndroid.show(`Canceled!`, ToastAndroid.SHORT)
            } else if (response.error) {
                ToastAndroid.show(`Sorry! Please Choose again!`, ToastAndroid.SHORT)
            } else {
                this.api_foto(response)
            }
        })
    }

    api_foto = (value) => {
        if (value.type === 'image/jpeg') {
            let photo = new FormData()
            photo.append(
                `image`, {
                uri: value.uri,
                name: value.fileName,
                type: value.type
            }
            )

            const api = async () => Axios.put(
                `https://app-citizenjournalism.herokuapp.com/api/v1/user/photo`,
                photo,
                {
                    headers: {
                        Authorization: `Bearer ${this.props.token.token}`
                    }
                }
            )

            api()
                .then(res => {
                    ToastAndroid.show(`Berhasil Update Foto!`, ToastAndroid.SHORT)
                })
        } else {
            ToastAndroid.show('Format harus JPEG!', ToastAndroid.SHORT)
        }
    }

    update = async () => {
        const updet = async (objParam) => await Axios.put(
            'https://app-citizenjournalism.herokuapp.com/api/v1/user/update',
            objParam,
            {
                headers: {
                    Authorization: `Bearer ${this.props.token.token}`
                }
            }
        )

        updet({
            email: this.state.email,
            fullname: this.state.fullname,
            gender: this.state.gender,
            address: this.state.address
        })
            .then(res => {
                ToastAndroid.show('Berhasil Update!', ToastAndroid.SHORT)
                this.setState({
                    Modal: false
                })
                this.props.navigation.pop()
            })
    }

    tutup = async () => {
        const del = async () => await Axios.delete(
            'https://app-citizenjournalism.herokuapp.com/api/v1/user/delete',
            {
                headers: {
                    Authorization: `Bearer ${this.props.token.token}`
                }
            }
        )

        del()
            .then(res => {
                Storage.setData('token', 'null')
                ToastAndroid.show('Logout', ToastAndroid.SHORT)
                this.setState({ Modal: false })
                this.props.navigation.navigate('Home')
            })
    }

    render() {
        return (
            <Container>
                <Edit_Header />
                <Item
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderColor: 'transparent',
                        flexDirection: 'column',
                        height: '20%'
                    }}
                >
                    <Card transparent>
                        <CardItem>
                            <Image
                                source={{ uri: this.state.picture }}
                                style={{
                                    width: 100,
                                    height: 100,
                                    borderRadius: 100 / 2
                                }}
                            />
                        </CardItem>
                    </Card>
                    <Button transparent
                        style={{
                            borderRadius: 10,
                            width: '80%',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                        onPress={this.Pict_Prof}
                    >
                        <Text
                            style={{
                                color: '#006AB9',
                                fontWeight: 'bold',
                                fontSize: 16
                            }}
                        >
                            Change Profile Picture
                            </Text>
                    </Button>
                </Item>
                <Content
                    style={{
                        display: 'flex',
                        backgroundColor: '#FFF'
                    }}
                >
                    <Item
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderColor: 'transparent',
                            flexDirection: 'column'
                        }}
                    >
                    </Item>
                    <Item
                        style={{
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderColor: 'transparent'
                        }}
                    >
                        <Text
                            style={{
                                color: 'black',
                                alignSelf: 'flex-start',
                                marginLeft: '10%',
                                fontSize: 16,
                                marginVertical: 10
                            }}
                        >
                            E-mail
                        </Text>
                        <Input
                            style={{
                                backgroundColor: '#DFDFDF',
                                width: '80%',
                            }}
                            value={this.state.email}
                            onChangeText={(text) => this.setState({ email: text })}
                        />
                        <Text
                            style={{
                                color: 'black',
                                alignSelf: 'flex-start',
                                marginLeft: '10%',
                                fontSize: 16,
                                marginVertical: 10
                            }}
                        >
                            Fullname
                        </Text>
                        <Input
                            style={{
                                backgroundColor: '#DFDFDF',
                                width: '80%'
                            }}
                            value={this.state.fullname}
                            onChangeText={(text) => this.setState({ fullname: text })}
                        />
                        <Text
                            style={{
                                color: 'black',
                                alignSelf: 'flex-start',
                                marginLeft: '10%',
                                fontSize: 16,
                                marginVertical: 10
                            }}
                        >
                            Gender
                        </Text>
                        <Item picker
                            style={{
                                width: '80%',
                                backgroundColor: '#DFDFDF'
                            }}
                        >
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                placeHolderText="Select your Gender"
                                style={{ color: "#33333" }}
                                selectedValue={this.state.gender}
                                onValueChange={(value) => this.setState({ gender: value })}
                            >
                                <Picker.Item label="Male" value='M' />
                                <Picker.Item label="Female" value='F' />
                            </Picker>
                        </Item>
                        <Text
                            style={{
                                color: 'black',
                                alignSelf: 'flex-start',
                                marginLeft: '10%',
                                fontSize: 16,
                                marginVertical: 10
                            }}
                        >
                            Address
                        </Text>
                        <Input
                            style={{
                                backgroundColor: '#DFDFDF',
                                width: '80%'
                            }}
                            value={this.state.address}
                            onChangeText={(text) => this.setState({ address: text })}
                        />
                        <Button
                            style={{
                                width: '60%',
                                borderRadius: 10,
                                marginVertical: 20,
                                backgroundColor: '#006AB9',
                                justifyContent: 'center'
                            }}
                            onPress={() => {
                                this.setState({
                                    Modal: true
                                })
                                this.update()
                            }}
                        >
                            <Text
                                style={{
                                    color: 'white',
                                    alignSelf: 'center'
                                }}
                            >
                                Update
                            </Text>
                        </Button>
                        <Button transparent
                            style={{
                                borderRadius: 10,
                                width: '80%',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                            onPress={
                                () => {
                                    this.setState({
                                        Modal: true
                                    })
                                    this.tutup()
                                }
                            }
                        >
                            <Text
                                style={{
                                    color: '#006AB9',
                                    fontWeight: 'bold'
                                }}
                            >
                                Delete Account
                                </Text>
                    </Button>
                    </Item>
                </Content>
                <Modal
                    animationType='fade'
                    transparent={true}
                    visible={this.state.Modal}
                    onRequestClose={
                        () => Alert.alert(`Success Login`)
                    }
                >
                    <View
                        style={{
                            backgroundColor: 'rgba(0,0,0,0.4)',
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Modal>
            </Container>
        )
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Edit))