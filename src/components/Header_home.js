import React, { Component } from 'react'
import { Image } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Header, Left, Icon, Body, Right, Text } from 'native-base'
import { connect } from 'react-redux'
const photos = require('../Logos/Logo_Hitam.png')

const mapStateToProps = state => ({
    token: state.token,
    user: state.user
})

class Header_home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            token: null
        }
    }

    render() {
        return (
            <Header
                style={{
                    display: 'flex',
                    backgroundColor: '#FFFFFF'
                }}
            >
                <Left
                    style={{
                        flex: 1
                    }}
                >
                    <Text
                        style={{
                            color: '#60A5D4'
                        }}
                        onPress={() =>{
                            this.props.navigation.navigate('Category')
                        }}
                    >
                        Category
                    </Text>
                </Left>
                <Body
                    style={{
                        flex: 1
                    }}
                >
                    <Image
                        source={photos}
                        style={{
                            width: '100%',
                            height: '100%'
                        }}
                    />
                </Body>
                <Right
                    style={{
                        flex: 1
                    }}
                >
                    <Icon
                        type="Ionicons"
                        name="md-search"
                        style={{
                            color: '#60A5D4'
                        }}
                        onPress={
                            () => {
                                this.props.navigation.navigate('Search')
                            }
                        }
                    />
                </Right>
            </ Header>
        )
    }
}
export default withNavigation(connect(mapStateToProps)(Header_home))