import React, { Component } from 'react'
import { withNavigation } from 'react-navigation'
import { Header, Left, Text, Body, Right } from 'native-base'
const photos = require('../Logos/Logo_Hitam.png')

class Header_about extends Component {
    render() {
        return (
            <Header
                style={{
                    display: 'flex',
                    backgroundColor: '#FFF'
                }}
            >
               <Left
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}
                >
                    <Text style={{color: '#006AB9', fontSize: 15}} onPress={() => this.props.navigation.goBack()}>
                        Cancel
                    </Text>
                </Left>
                <Body
                    style={{
                        flex: 1
                    }}
                >
                    <Text style={{fontWeight: 'bold', alignSelf: 'center'}}>About</Text>
                </Body>
                <Right />
            </ Header>
        )
    }
}
export default withNavigation(Header_about)