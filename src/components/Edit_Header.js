import React, { Component } from 'react'
import { Text, Image } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Header, Left, Icon, Body, Right } from 'native-base'

class Edit_Header extends Component {
    render() {
        return (
            <Header
                style={{
                    display: 'flex',
                    backgroundColor: '#FFF'
                }}
            >
                <Left
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}
                >
                    <Text style={{color: '#006AB9', fontSize: 15}} onPress={() => this.props.navigation.goBack()}>
                        Cancel
                    </Text>
                </Left>
                <Body
                    style={{
                        flex: 1
                    }}
                >
                    <Text style={{fontSize: 15, fontWeight: "bold", alignSelf: 'center'}} onPress={() => this.props.navigation.goBack()}>
                    Edit Profile
                    </Text>
                </Body>
                <Right
                    style={{
                        flex: 1
                    }}
                />        
            </Header>
        )
    }
}

export default withNavigation(Edit_Header)