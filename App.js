import React, { Component } from 'react'
import configureStore from './store'
import { Provider } from 'react-redux'
import { StatusBar } from 'react-native'
import AppContainer from './Stack'
const store = configureStore()

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <StatusBar
          backgroundColor='#EAD263'
          barStyle='light-content'
          hidden={true}
          translucent={true}
        />
        <AppContainer />
      </Provider>
    )
  }
}
